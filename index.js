'use strict'

var fs = require('fs');
var express = require('express');

var favIconHtml = '<link rel="icon" type="image/png" href="./favicon-32x32.png" sizes="32x32" />' +
                  '<link rel="icon" type="image/png" href="./favicon-16x16.png" sizes="16x16" />'

var swaggerInit
var swaggerCss
var swaggerBundle
var swaggerStandalone
var htmlWithOptionsOrg

var generateHTML = function (swaggerDoc, opts, options, customCss, customfavIcon, swaggerUrl, customSiteTitle) {  
  var isExplorer
  var customJs
  if (opts && typeof opts === 'object') {
    isExplorer = opts.explorer
    options = opts.swaggerOptions
    customCss = opts.customCss
    customJs = opts.customJs
    customfavIcon = opts.customfavIcon
    swaggerUrl = opts.swaggerUrl
    customSiteTitle = opts.customSiteTitle
  } else {
    //support legacy params based function
    isExplorer = opts
  }
	options = options || {};
  var explorerString = isExplorer ? '' : '.swagger-ui .topbar .download-url-wrapper { display: none }';
    customCss = explorerString + ' ' + customCss || explorerString;
    customfavIcon = customfavIcon || false;    
    customSiteTitle = customSiteTitle || 'Swagger UI';
	var html = fs.readFileSync(__dirname + '/indexTemplate.html');
    try {
    	fs.unlinkSync(__dirname + '/index.html');
    } catch (e) {

    }

    var favIconString = customfavIcon ? '<link rel="icon" href="' + customfavIcon + '" />' : favIconHtml;
    var htmlWithCustomCss = html.toString().replace('<% customCss %>', customCss);
    var htmlWithFavIcon = htmlWithCustomCss.replace('<% favIconString %>', favIconString);
    var htmlWithCustomJs = htmlWithFavIcon.replace('<% customJs %>', customJs ? `<script src="${customJs}"></script>` : '');

    var initOptions = {
      swaggerDoc: swaggerDoc || undefined,
      customOptions: options,
      swaggerUrl: swaggerUrl || undefined,
      allowedAPIVersions: opts.allowedAPIVersions,
      latestAPIVersion: opts.latestAPIVersion
    }
    var js = fs.readFileSync(__dirname + '/swagger-ui-init.js');
    swaggerInit = js.toString().replace('<% swaggerOptions %>', stringify(initOptions))    
    var css = fs.readFileSync(__dirname + '/static/swagger-ui.css');
    swaggerCss = css.toString()
    var bundle = fs.readFileSync(__dirname + '/static/swagger-ui-bundle.js');
    swaggerBundle = bundle.toString()
    var standalone = fs.readFileSync(__dirname + '/static/swagger-ui-standalone-preset.js');
    swaggerStandalone = standalone.toString()
    return htmlWithCustomJs //.replace('<% title %>', customSiteTitle)
}

var setup = function (swaggerDoc, opts, options, customCss, customfavIcon, swaggerUrl, customSiteTitle) {
    var htmlWithOptions = generateHTML(swaggerDoc, opts, options, customCss, customfavIcon, swaggerUrl, customSiteTitle)    
    return function (req, res) {
      if(req.tempStore.admin) {
        if (htmlWithOptions.indexOf('<% title %>') != -1) {
            htmlWithOptionsOrg = htmlWithOptions;
        }
        htmlWithOptions = htmlWithOptionsOrg.replace('<% title %>', 'Qliktag')
      } 
      else if (req && req.tempStore && req.tempStore.controllerInfo) {        
        if (htmlWithOptions.indexOf('<% title %>') != -1) {
            htmlWithOptionsOrg = htmlWithOptions;
        }
        htmlWithOptions = htmlWithOptionsOrg.replace('<% title %>', req.tempStore.controllerInfo.controllerName)

      }
      res.send(htmlWithOptions) 
    };
};

function swaggerInitFn (req, res, next) {  
  if (req.url === '/swagger-ui-init.js') {    
    res.setHeader('Content-Type', 'application/javascript');    
    res.send(swaggerInit)
  } else if(req.url.includes('/swagger-ui.css')){    
    res.setHeader('Content-Type', 'text/css')        
    res.send(swaggerCss)    
  } else if(req.url.includes('/swagger-ui-bundle.js')){    
    res.setHeader('Content-Type', 'application/javascript')        
    res.send(swaggerBundle)
  }
  else if( req.url.includes('/swagger-ui-standalone-preset.js')){    
    res.setHeader('Content-Type', 'application/javascript')        
    res.send(swaggerStandalone)
  } else {    
    next()
  }
}

var swaggerInitFunction = function (swaggerDoc, opts) {    
  var js = fs.readFileSync(__dirname + '/swagger-ui-init.js');
  var swaggerInitFile = js.toString().replace('<% swaggerOptions %>', stringify(opts))
  return function (req, res, next) {
    if (req.url === '/swagger-ui-init.js') {
      res.set('Content-Type', 'application/javascript')
      res.send(swaggerInitFile)
    } else {
      next()
    }
  }
} 

var serveFiles = function (swaggerDoc, opts) {    
  opts = opts || {}
  var initOptions = {
    swaggerDoc: swaggerDoc || undefined,
    customOptions: opts.swaggerOptions || {},
    swaggerUrl: opts.swaggerUrl || {}
  }
  var swaggerInitWithOpts = swaggerInitFunction(swaggerDoc, initOptions)
  return [swaggerInitWithOpts, express.static(__dirname + '/static')]
}

var serve = [swaggerInitFn, express.static(__dirname + '/static')];
var serveWithOptions = options => [swaggerInitFn, express.static(__dirname + '/static', options)];

var stringify = function (obj, prop) {
  var placeholder = '____FUNCTIONPLACEHOLDER____';
  var fns = [];
  var json = JSON.stringify(obj, function (key, value) {
    if (typeof value === 'function') {
      fns.push(value);
      return placeholder;
    }
    return value;
  }, 2);
  json = json.replace(new RegExp('"' + placeholder + '"', 'g'), function (_) {
    return fns.shift();
  });
  return 'var options = ' + json + ';';
};

module.exports = {
	setup: setup,
	serve: serve,
  serveWithOptions: serveWithOptions,
  generateHTML: generateHTML,
  serveFiles: serveFiles
};
