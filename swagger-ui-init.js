window.onload = function() {
    localStorage.removeItem('oauth2Token');
    localStorage.removeItem('oauth2RefreshToken');
    localStorage.removeItem('oauth2AssociatedAccountsInfo');
    localStorage.removeItem('selectedAccountId');
    // Build a system
    var url = window.location.search.match(/url=([^&]+)/);
    if (url && url.length > 1) {
        url = decodeURIComponent(url[1]);
    } else {
        url = window.location.origin;
    }
    <% swaggerOptions %>
    url = options.swaggerUrl || url
    let adminUrl = false;
    let adminUrlRegex = new RegExp('admin(\.[ldts])?\.qliktag\.com');
    let adminUrlRegexCheck = adminUrlRegex.test(window.location.host);
    let adminUrlRegexIO = new RegExp('admin(\.[ldts])?\.qliktag\.io');
    let adminUrlRegexCheckIO = adminUrlRegexIO.test(window.location.host);
    if(adminUrlRegexCheck || adminUrlRegexCheckIO){
      adminUrl = true;
    }
    if(!adminUrl){
    let urlList = url;
    let swaggerUrlsData = [];    
    let splitRequestURL = window.location.pathname.split('/');
    let selectedVersion = options.latestAPIVersion;

    if (splitRequestURL.length > 0 && splitRequestURL.length > 2) {      
      if(options.allowedAPIVersions.indexOf(splitRequestURL[2])!=-1){
         selectedVersion = splitRequestURL[2];
      }
    }   
    
    urlList.forEach(function(data){
        if(data.version == selectedVersion){
            swaggerUrlsData.push(data)
        }
    });

    urlList.forEach(function(d){
        let found = false
        swaggerUrlsData.forEach(function(s){
            if(s.version == d.version){
                found = true
            }
        });
        if(!found){
            swaggerUrlsData.push(d)
        }
    });        
    url = swaggerUrlsData
    }
    var customOptions = options.customOptions
    var spec1 = options.swaggerDoc
    var swaggerOptions = {
        spec: spec1,
        urls: url,
        dom_id: '#swagger-ui',
        deepLinking: true,
        presets: [
            SwaggerUIBundle.presets.apis,
            SwaggerUIStandalonePreset
        ],
        plugins: [
            SwaggerUIBundle.plugins.DownloadUrl
        ],
        layout: "StandaloneLayout"
    }
    for (var attrname in customOptions) {
        swaggerOptions[attrname] = customOptions[attrname];
    }
    var ui = SwaggerUIBundle(swaggerOptions)

    if (customOptions.oauth) {
        ui.initOAuth(customOptions.oauth)
    }

    if (customOptions.authAction) {
        ui.authActions.authorize(customOptions.authAction)
    }

    window.ui = ui
}
