### QlibSwaggerUiExpress

## What is this repository for?
QlibSwaggerUiExpress is used in QliktagAPI project for purpose of loading swagger doc on configured url

## Setting up
any update done on QlibSwaggerUiDist(which is dist folder of QlibSwaggerUi) needs get pulled by QlibSwaggerUiExpress and the version number of QlibSwaggerUiExpress should get updated in QliktagAPI package.lock json

# update on setting up 
QlibSwaggerUiExpress uses QlibSwaggerUiDist files which needs to be copied to static folder if there are any changes made in QlibSwaggerUi


## TO-DO
update QlibSwaggerUiExpress to use latest version of swaggeruiexpress npm library to be up to date 

